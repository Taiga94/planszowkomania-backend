package pl.inzyniery.puszek;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import pl.inzyniery.puszek.config.DatabaseConfig;

import javax.persistence.Persistence;

@SpringBootApplication
@EnableOAuth2Client
@EnableAutoConfiguration
@ComponentScan(basePackages = {"pl.inzyniery.puszek", "pl.inzyniery.puszek.config",
        "pl.inzyniery.puszek.repository", "pl.inzyniery.puszek.controller"})
@IntegrationComponentScan(basePackages = {"pl.fewbits.puszek", "pl.fewbits.puszek.config"})
public class Application extends SpringBootServletInitializer implements CommandLineRunner {

    @Autowired
    private DatabaseConfig databaseConfig;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    public static void main(String... args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        Persistence.generateSchema("internal", databaseConfig.persistenceMap());
    }


}
