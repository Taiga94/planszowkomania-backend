package pl.inzyniery.puszek.api.chat;

public class AddChatRequest {
    public String name;
    public double latitude;
    public double longitude;
    public double range;
    public int maxUsersNumber;
}
