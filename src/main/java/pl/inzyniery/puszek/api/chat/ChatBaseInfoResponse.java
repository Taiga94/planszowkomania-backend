package pl.inzyniery.puszek.api.chat;

import pl.inzyniery.puszek.model.Chat;

public class ChatBaseInfoResponse {
    public String id;
    public String name;
    public double latitude;
    public double longitude;
    public double rangeInMeters;
    public int maxUsersNumber;

    public ChatBaseInfoResponse(Chat chat) {
        id = String.valueOf(chat.getId());
        name = chat.getName();
        latitude = chat.getLatitude();
        longitude = chat.getLongitude();
        rangeInMeters = chat.getRangeInMeters();
        maxUsersNumber = chat.getMaxUsersNumber();
    }
}
