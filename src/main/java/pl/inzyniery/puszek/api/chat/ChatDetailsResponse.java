package pl.inzyniery.puszek.api.chat;

import pl.inzyniery.puszek.model.Chat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by viader on 10.12.16.
 */
public class ChatDetailsResponse extends ChatBaseInfoResponse {

    public List<MessageResponse> messages = new ArrayList<>();

    public ChatDetailsResponse(Chat chat) {
        super(chat);
    }
}
