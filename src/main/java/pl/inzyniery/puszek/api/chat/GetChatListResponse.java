package pl.inzyniery.puszek.api.chat;

import java.util.ArrayList;
import java.util.List;

public class GetChatListResponse {
    public List<ChatBaseInfoResponse> chats = new ArrayList<>();
}
