package pl.inzyniery.puszek.api.chat;

/**
 * Created by viader on 10.12.16.
 */
public class MessageResponse {

    private String author;
    private String textMessage;
    private String time;

    public MessageResponse(String author, String textMessage, String time) {
        this.author = author;
        this.textMessage = textMessage;
        this.time = time;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
