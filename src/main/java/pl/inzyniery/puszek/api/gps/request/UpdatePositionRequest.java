package pl.inzyniery.puszek.api.gps.request;

public class UpdatePositionRequest {
    public double latitude;
    public double longitude;
}
