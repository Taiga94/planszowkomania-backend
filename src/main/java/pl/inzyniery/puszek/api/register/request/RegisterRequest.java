package pl.inzyniery.puszek.api.register.request;

public class RegisterRequest {
    public String email;
    public String login;
    public String password;
}
