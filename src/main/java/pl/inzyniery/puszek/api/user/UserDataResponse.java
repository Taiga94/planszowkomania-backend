package pl.inzyniery.puszek.api.user;

import pl.inzyniery.puszek.model.User;

public class UserDataResponse {
    public String login;
    public String email;
    public String name;
    public double latitude;
    public double longitude;

    public UserDataResponse(User user) {
        name = user.getName();
        email = user.getEmail();
        login = user.getLogin();
        latitude = user.getLatitude();
        longitude = user.getLongitude();
    }
}
