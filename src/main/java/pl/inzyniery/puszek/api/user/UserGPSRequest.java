package pl.inzyniery.puszek.api.user;

public class UserGPSRequest {
    public double latitude;
    public double longitude;
    public double distance;
}
