package pl.inzyniery.puszek.config;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CopyServletOutputStream extends ServletOutputStream {

    private DataOutputStream stream;
    private String copy;

    public CopyServletOutputStream(OutputStream output) {
        stream = new DataOutputStream(output);
        copy = new String();
    }

    public void write(int b) throws IOException {
        stream.write(b);
        copy += b;
    }

    public void write(byte[] b) throws IOException  {
        stream.write(b);
        copy += b.toString();
    }

    public void write(byte[] b, int off, int len) throws IOException  {
        stream.write(b,off,len);
        copy += b.toString();
    }

    public String getCopy() {
        return copy;
    }

    @Override
    public boolean isReady() {
        return false;
    }

    @Override
    public void setWriteListener(WriteListener writeListener) {

    }
}