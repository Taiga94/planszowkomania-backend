package pl.inzyniery.puszek.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.util.Enumeration;

public class LoggerInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(LoggerInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\n").
                append(request.getMethod()).
                append(" ").
                append(request.getRequestURL()).
                append("\n");

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            stringBuilder.append(headerName).append(": ").append(request.getHeader(headerName)).append("\n");
        }

        Enumeration params = request.getParameterNames();
        if (params.hasMoreElements()) {
            stringBuilder.append("Parameters:").append("\n");
        }
        while (params.hasMoreElements()) {
            String paramName = (String) params.nextElement();
            stringBuilder.append(paramName + ": " + request.getParameter(paramName)).append("\n");
        }

        if(request instanceof MultiReadHttpServletRequest) {
            try {
                String line;
                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null)
                    stringBuilder.append(line).append("\n");
            } catch (Exception e) {
                logger.error("Exception in LoggerInterceptor::preHandle()!\n" + e.getLocalizedMessage());
            }
        }

        logger.info(stringBuilder.toString());

        return super.preHandle(request, response, handler);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("\n").
                append("Response(").
                append(response.getStatus()).
                append(") ").
                append(request.getRequestURL()).
                append("\n");

        if(response instanceof MultiWriteHttpServletResponse) {
            stringBuilder.append(new String(((MultiWriteHttpServletResponse) response).getData()));
        }

        logger.info(stringBuilder.toString());

        super.afterCompletion(request, response, handler, ex);
    }
}