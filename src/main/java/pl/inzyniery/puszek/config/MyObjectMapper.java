package pl.inzyniery.puszek.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class MyObjectMapper extends ObjectMapper {
    public MyObjectMapper() {
        setSerializationInclusion(JsonInclude.Include.NON_NULL);
        SimpleDateFormat sfd = new SimpleDateFormat("dd MM yyyy HH:mm:ss");
        setDateFormat(sfd);
    }
}