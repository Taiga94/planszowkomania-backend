package pl.inzyniery.puszek.config;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

public class RequestWrapperFilter implements Filter {
    public void init(FilterConfig config)
            throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws java.io.IOException, ServletException {
        if(request instanceof HttpServlet && response instanceof HttpServlet) {
            OutputStream out = response.getOutputStream();
            MultiReadHttpServletRequest requestWrapper = new MultiReadHttpServletRequest((HttpServletRequest) request);
            MultiWriteHttpServletResponse responseWrapper = new MultiWriteHttpServletResponse((HttpServletResponse) response);
            chain.doFilter(requestWrapper, responseWrapper);
            byte responseContent[] = responseWrapper.getData();
            out.write(responseContent);
            out.close();
        }
        else {
            chain.doFilter(request, response);
        }

    }

    public void destroy() {
    }
}