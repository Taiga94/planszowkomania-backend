package pl.inzyniery.puszek.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import pl.inzyniery.puszek.api.chat.MessageRequest;
import pl.inzyniery.puszek.api.chat.MessageResponse;
import pl.inzyniery.puszek.controller.WSChatController;

@Configuration
@EnableWebSocketMessageBroker
@EnableIntegration
@IntegrationComponentScan(basePackages = {"pl.inzyniery.puszek", "pl.inzyniery.puszek.config"})
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    @Autowired
    MyObjectMapper objectMapper;
    @Autowired
    WSChatController chatController;
    @Autowired
    ConfigurableApplicationContext context;

    @Bean
    public MessageChannel mqttInputChannel() {
        return new DirectChannel();
    }

    @Bean
    public MqttPahoClientFactory mqttClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        factory.setServerURIs("tcp://localhost:1883");
        return factory;
    }

    @Bean
    public MqttPahoMessageDrivenChannelAdapter inbound() {
        MqttPahoMessageDrivenChannelAdapter adapter =
                new MqttPahoMessageDrivenChannelAdapter("serverMqtt", mqttClientFactory(), "test");
        adapter.setCompletionTimeout(5000);
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(1);
        adapter.setOutputChannel(mqttInputChannel());
        return adapter;
    }

    @Bean
    @ServiceActivator(inputChannel = "mqttInputChannel")
    public MessageHandler handler() {
        final String inputChannelName = "chatRequest/";
        final String outputChannelName = "chatResponse/";
        return message -> {
            String topic = (String) message.getHeaders().get(MqttHeaders.TOPIC);
            if (topic.startsWith(inputChannelName)) {
                String id = topic.replaceFirst(inputChannelName, "");
                try {
                    MessageRequest request = objectMapper.readValue(message.getPayload().toString(), MessageRequest.class);
                    MessageResponse response = chatController.sendMessageToChat(request, Integer.valueOf(id));
                    MyGateway gateway = context.getBean(MyGateway.class);
                    gateway.sendToMqtt(MessageBuilder.withPayload(objectMapper.writeValueAsString(response)).
                            setHeader(MqttHeaders.TOPIC, outputChannelName + id).
                            build());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Bean
    @ServiceActivator(inputChannel = "mqttOutboundChannel")
    public MessageHandler mqttOutbound() {
        MqttPahoMessageHandler messageHandler =
                new MqttPahoMessageHandler("testClient", mqttClientFactory());
        messageHandler.setAsync(true);
        messageHandler.setDefaultTopic("testTopic");
        return messageHandler;
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/topic");
        registry.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry stompEndpointRegistry) {
        stompEndpointRegistry.addEndpoint("/chat").withSockJS();
    }

    @MessagingGateway(defaultRequestChannel = "mqttOutboundChannel")
    public interface MyGateway {
        void sendToMqtt(Message data);
    }
}
