package pl.inzyniery.puszek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.web.bind.annotation.*;
import pl.inzyniery.puszek.api.chat.*;
import pl.inzyniery.puszek.model.Chat;
import pl.inzyniery.puszek.model.MessageModel;
import pl.inzyniery.puszek.model.Token;
import pl.inzyniery.puszek.model.User;
import pl.inzyniery.puszek.repository.ChatRepository;
import pl.inzyniery.puszek.repository.TokenRepository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ChatController {

    @Autowired
    @Resource
    MqttPahoMessageDrivenChannelAdapter adapter;
    @Autowired
    @Resource
    ChatRepository repository;
    @Autowired
    @Resource
    TokenRepository tokenRepository;

    @RequestMapping(value = "/addChat", method = RequestMethod.POST)
    public ResponseEntity addChat(@RequestHeader("token") String tokenContent, @RequestBody AddChatRequest request) {
        Token token = tokenRepository.findByContent(tokenContent);
        if (token == null || token.getUser() == null) {
            return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
        }
        User user = token.getUser();
        Chat chat = new Chat();
        chat.setName(request.name);
        chat.setLatitude(request.latitude);
        chat.setLongitude(request.longitude);
        chat.setRangeInMeters(request.range);
        chat.setMaxUsersNumber(request.maxUsersNumber);
        chat.setOwner(user);
        chat = repository.saveAndFlush(chat);
        adapter.addTopic("chatRequest/" + chat.getId(), 1);
        return new ResponseEntity<>(new ChatDetailsResponse(chat), HttpStatus.OK);
    }

    @RequestMapping(value = "/getChatList", method = RequestMethod.POST)
    public GetChatListResponse getChatList(@RequestBody GetChatListRequest request) {
        List<Chat> chatList = repository.getChatList(request);
        GetChatListResponse response = new GetChatListResponse();

        response.chats.addAll(chatList.stream().map(ChatBaseInfoResponse::new).collect(Collectors.toList()));
        return response;
    }

    @RequestMapping(value = "api/getChatDetails", method = RequestMethod.POST)
    public ChatDetailsResponse getChatDetails(@RequestBody ChatDetailsRequest chatDetailsRequest) {
        Chat chat = repository.findById(chatDetailsRequest.id);
        if (chat != null) {
            ChatDetailsResponse chatDetailsResponse = new ChatDetailsResponse(chat);

            List<MessageModel> messages = new ArrayList(chat.getMessages());
            Collections.sort(messages, MessageModel.MessageModelComparator);
            for (MessageModel model : messages) {
                if (model.getAuthor().getName() != null && !model.getAuthor().getName().isEmpty()) {
                    chatDetailsResponse.messages.add(new MessageResponse(model.getAuthor().getName(), model.getText(), model.getDate()));
                } else {
                    chatDetailsResponse.messages.add(new MessageResponse(model.getAuthor().getLogin(), model.getText(), model.getDate()));
                }
            }
            return chatDetailsResponse;
        }
        return null;
    }

}
