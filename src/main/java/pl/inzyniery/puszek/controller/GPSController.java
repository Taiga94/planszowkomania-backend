package pl.inzyniery.puszek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.inzyniery.puszek.api.gps.request.UpdatePositionRequest;
import pl.inzyniery.puszek.api.login.request.AnonymousLoginRequest;
import pl.inzyniery.puszek.api.login.request.LoginRequest;
import pl.inzyniery.puszek.api.login.response.LoginResponse;
import pl.inzyniery.puszek.model.Token;
import pl.inzyniery.puszek.model.User;
import pl.inzyniery.puszek.repository.TokenRepository;
import pl.inzyniery.puszek.repository.UserRepository;
import pl.inzyniery.puszek.util.NameGenerator;
import pl.inzyniery.puszek.util.UserUtil;

import java.util.UUID;

@RestController
public class GPSController {

    @Autowired
    UserRepository repository;
    @Autowired
    TokenRepository tokenRepository;

    @RequestMapping(value = "/updatePosition", method = RequestMethod.POST)
    public void updatePosition(@RequestHeader("token") String tokenContent, @RequestBody UpdatePositionRequest request) {
        Token token = tokenRepository.findByContent(tokenContent);
        if (token != null && token.getUser() != null) {
            token.getUser().setLatitude(request.latitude);
            token.getUser().setLongitude(request.longitude);
            repository.saveAndFlush(token.getUser());
        }
    }
}

