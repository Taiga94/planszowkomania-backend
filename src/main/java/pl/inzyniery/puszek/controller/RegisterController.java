package pl.inzyniery.puszek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.inzyniery.puszek.api.register.request.RegisterRequest;
import pl.inzyniery.puszek.model.User;
import pl.inzyniery.puszek.model.UserActivation;
import pl.inzyniery.puszek.repository.UserActivationRepository;
import pl.inzyniery.puszek.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

@RestController
public class RegisterController {

    @Autowired
    private UserRepository repository;
    @Autowired
    private UserActivationRepository userActivationRepository;
    @Autowired
    private MailSender mailSender;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public void doRegistration(@RequestBody RegisterRequest request, HttpServletRequest baseRequest) throws UnknownHostException {
        User user = new User();
        user.setEmail(request.email);
        user.setPassword(request.password);
        user.setLogin(request.login);
        user.setName(request.login);
        repository.saveAndFlush(user);

        String activationCode = UUID.randomUUID().toString().replace("-", "");
        UserActivation userActivation = new UserActivation();
        userActivation.setUser(user);
        userActivation.setActivationCode(activationCode);
        userActivationRepository.saveAndFlush(userActivation);

        SimpleMailMessage smm = new SimpleMailMessage();
        smm.setTo(request.email);
        smm.setSubject("[Planszówkomania] Your account has been registered!");
        smm.setText("Your account " + request.login + " has been registered successfully! " +
                "Activate your account here: http://" + InetAddress.getLocalHost() + ":8080/planszowkomania/activationAccount?code=" + activationCode);
        mailSender.send(smm);
    }
}
