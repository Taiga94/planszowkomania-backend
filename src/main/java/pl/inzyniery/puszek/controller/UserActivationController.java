package pl.inzyniery.puszek.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import pl.inzyniery.puszek.model.User;
import pl.inzyniery.puszek.model.UserActivation;
import pl.inzyniery.puszek.repository.UserActivationRepository;
import pl.inzyniery.puszek.repository.UserRepository;

@Controller
public class UserActivationController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserActivationRepository userActivationRepository;

    @RequestMapping("/activationAccount")
    public ModelAndView activateUser(@RequestParam(value = "code") String code) throws Exception {
        UserActivation userActivation = userActivationRepository.findByActivationCode(code);
        if (userActivation != null) {
            activateUser(userActivation);
            return new ModelAndView("activationPage")
                    .addObject("title", "Account activated")
                    .addObject("message", "Your account has been activated successfully and now you can login!");
        }
        throw new Exception("Activation error.");
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ModelAndView exceptionHandler(Exception ex) {
        return new ModelAndView("activationPage").addObject("title", "Error occured").addObject("message", ex.getCause());
    }

    private void activateUser(@NotNull UserActivation userActivation) {
        User user = userActivation.getUser();
        if (user != null && !user.isActive()) {
            user.setActive(true);
            userRepository.saveAndFlush(user);
        }
    }
}
