package pl.inzyniery.puszek.controller;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;
import pl.inzyniery.puszek.api.login.request.AnonymousLoginRequest;
import pl.inzyniery.puszek.api.login.request.LoginRequest;
import pl.inzyniery.puszek.api.login.response.LoginResponse;
import pl.inzyniery.puszek.api.user.UserDataResponse;
import pl.inzyniery.puszek.api.user.UserGPSRequest;
import pl.inzyniery.puszek.api.user.UserLookupRequest;
import pl.inzyniery.puszek.api.user.UserResponse;
import pl.inzyniery.puszek.model.Token;
import pl.inzyniery.puszek.model.User;
import pl.inzyniery.puszek.repository.TokenRepository;
import pl.inzyniery.puszek.repository.UserRepository;
import pl.inzyniery.puszek.util.NameGenerator;
import pl.inzyniery.puszek.util.UserUtil;

import java.util.UUID;

@RestController
public class UserController {

    @Autowired
    UserRepository repository;
    @Autowired
    TokenRepository tokenRepository;

    @RequestMapping("/getUsers")
    public UserResponse getUsers() {
        UserResponse response = new UserResponse();
        for(User user : repository.getUsers()) {
            response.data.add(new UserDataResponse(user));
        }
        return response;
    }

    @RequestMapping(value = "/getUsersLookup", method = RequestMethod.POST)
    public UserResponse getUsersLookup(@RequestBody UserLookupRequest request) {
        UserResponse response = new UserResponse();
        for(User user : repository.getUsersLookup(request.name)) {
            if(!user.getName().equals(request.name)) {
                response.data.add(new UserDataResponse(user));
            }
        }
        return response;
    }

    @RequestMapping(value = "/getUsersGPS", method = RequestMethod.POST)
    public UserResponse getUsersGPS(@RequestHeader("token") String tokenContent, @RequestBody UserGPSRequest request) {
        UserResponse response = new UserResponse();
        Token token = tokenRepository.findByContent(tokenContent);
        if (token != null && token.getUser() != null) {
            for(User user : repository.getUsersGPS(request.distance, token.getUser().getLatitude(), token.getUser().getLongitude())) {
                if(!user.getName().equals(token.getUser().getName())) {
                    response.data.add(new UserDataResponse(user));
                }
            }
        }
        return response;
    }

    @RequestMapping(value = "/anonymousLogin", method = RequestMethod.POST)
    public LoginResponse doLogin(@RequestBody AnonymousLoginRequest request) {
        User user = repository.findByUid(request.uid);
        if (user == null) {
            user = new User();
            user.setUid(request.uid);
            user.setName(NameGenerator.generateName());
            repository.saveAndFlush(user);
        }

        return loginUser(user);
    }

    @RequestMapping("/login")
    public LoginResponse doLogin(@RequestBody LoginRequest request) {
        User user = UserUtil.getUserByEmailOrLogin(repository, request.loginOrEmail);
        if (user != null && request.password.equals(user.getPassword()) && user.isActive()) {
            return loginUser(user);
        }
        return null;
    }

    private LoginResponse loginUser(User user) {
        String activationCode = UUID.randomUUID().toString().replace("-", "");
        Token token = new Token();
        token.setUser(user);
        token.setContent(user.getName() + "-" + activationCode);
        token = tokenRepository.saveAndFlush(token);
        user.getTokens().add(token);
        repository.saveAndFlush(user);

        LoginResponse loginResponse = new LoginResponse();
        loginResponse.name = user.getName();
        loginResponse.token = token.getContent();
        return loginResponse;
    }

}

