package pl.inzyniery.puszek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import pl.inzyniery.puszek.api.chat.MessageRequest;
import pl.inzyniery.puszek.api.chat.MessageResponse;
import pl.inzyniery.puszek.model.Chat;
import pl.inzyniery.puszek.model.JoinUserChat;
import pl.inzyniery.puszek.model.MessageModel;
import pl.inzyniery.puszek.model.Token;
import pl.inzyniery.puszek.repository.*;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class WSChatController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    ChatRepository chatRepository;
    @Autowired
    TokenRepository tokenRepository;
    @Autowired
    JoinUserChatRepository joinUserChatRepository;
    @Autowired
    MessageRepository messageRepository;

    @MessageMapping("/chat")
    @SendTo("/topic/messages")
    public MessageResponse send(MessageRequest message) throws Exception {
        String time = new SimpleDateFormat("HH:mm").format(new Date());

        Token token = tokenRepository.findByContent(message.getTokenContent());
        if (token != null && token.getUser() != null) {
            Chat chat = chatRepository.findById(1);
            boolean isUserAssignedToChat = false;

            if (chat == null) {
                chat = new Chat();
                chat = chatRepository.saveAndFlush(chat);
            } else {
                for (JoinUserChat joinUserChat : chat.getUsers()) {
                    if (joinUserChat.getUser().getId() == token.getUser().getId()) {
                        isUserAssignedToChat = true;
                        break;
                    }
                }
            }

            if (!isUserAssignedToChat) {
                JoinUserChat joinUserChat = new JoinUserChat();
                joinUserChat.setUser(token.getUser());
                joinUserChat.setChat(chat);
                joinUserChat = joinUserChatRepository.saveAndFlush(joinUserChat);

                chat.getUsers().add(joinUserChat);
                token.getUser().getChats().add(joinUserChat);
                userRepository.saveAndFlush(token.getUser());
                chat = chatRepository.saveAndFlush(chat);
            }

            MessageModel messageModel = new MessageModel();
            messageModel.setChat(chat);
            messageModel.setAuthor(token.getUser());
            messageModel.setText(message.getTextMessage());
            messageModel.setDate(time);
            messageModel = messageRepository.saveAndFlush(messageModel);

            chat.getMessages().add(messageModel);
            chatRepository.saveAndFlush(chat);

            return new MessageResponse(message.getAuthor(), message.getTextMessage(), time);
        }
        return new MessageResponse(message.getAuthor(), message.getTextMessage(), time);
    }

    public MessageResponse sendMessageToChat(MessageRequest message, int id) throws Exception {
        String time = new SimpleDateFormat("HH:mm").format(new Date());

        Token token = tokenRepository.findByContent(message.getTokenContent());
        if (token != null && token.getUser() != null) {
            Chat chat = chatRepository.findById(id);
            boolean isUserAssignedToChat = false;

            if (chat == null) {
                chat = new Chat();
                chat = chatRepository.saveAndFlush(chat);
            } else {
                for (JoinUserChat joinUserChat : chat.getUsers()) {
                    if (joinUserChat.getUser().getId() == token.getUser().getId()) {
                        isUserAssignedToChat = true;
                        break;
                    }
                }
            }

            if (!isUserAssignedToChat) {
                JoinUserChat joinUserChat = new JoinUserChat();
                joinUserChat.setUser(token.getUser());
                joinUserChat.setChat(chat);
                joinUserChat = joinUserChatRepository.saveAndFlush(joinUserChat);

                chat.getUsers().add(joinUserChat);
                token.getUser().getChats().add(joinUserChat);
                userRepository.saveAndFlush(token.getUser());
                chat = chatRepository.saveAndFlush(chat);
            }

            MessageModel messageModel = new MessageModel();
            messageModel.setChat(chat);
            messageModel.setAuthor(token.getUser());
            messageModel.setText(message.getTextMessage());
            messageModel.setDate(time);
            messageModel = messageRepository.saveAndFlush(messageModel);

            chat.getMessages().add(messageModel);
            chatRepository.saveAndFlush(chat);

            return new MessageResponse(message.getAuthor(), message.getTextMessage(), time);
        }
        return new MessageResponse(message.getAuthor(), message.getTextMessage(), time);
    }

}
