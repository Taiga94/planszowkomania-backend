package pl.inzyniery.puszek.model;


import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity(name = "Chat")
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "latitude")
    private double latitude;
    @Column(name = "longitude")
    private double longitude;
    @Column(name = "rangeInMeters")
    private double rangeInMeters;
    @Column(name = "maxUsersNumber")
    private int maxUsersNumber;
    @ManyToOne
    @JoinColumn(name = "ownerId")
    User owner;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<MessageModel> messages = new LinkedHashSet<>();
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<JoinUserChat> users = new LinkedHashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getRangeInMeters() {
        return rangeInMeters;
    }

    public void setRangeInMeters(double rangeInMeters) {
        this.rangeInMeters = rangeInMeters;
    }

    public int getMaxUsersNumber() {
        return maxUsersNumber;
    }

    public void setMaxUsersNumber(int maxUsersNumber) {
        this.maxUsersNumber = maxUsersNumber;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Set<MessageModel> getMessages() {
        return messages;
    }

    public void setMessages(Set<MessageModel> messages) {
        this.messages = messages;
    }

    public Set<JoinUserChat> getUsers() {
        return users;
    }
}
