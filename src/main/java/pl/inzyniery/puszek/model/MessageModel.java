package pl.inzyniery.puszek.model;

import javax.persistence.*;
import java.util.Comparator;

@Entity
public class MessageModel implements Comparable<MessageModel> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User author;
    @Column(name = "text")
    private String text;
    @Column(name = "date_message")
    private String date;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chat_id")
    private Chat chat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int compareTo(MessageModel messageModel) {
        return getId() - messageModel.getId();
    }

    public static Comparator<MessageModel> MessageModelComparator
            = (messageModel1, messageModel2) -> messageModel1.compareTo(messageModel2);
}
