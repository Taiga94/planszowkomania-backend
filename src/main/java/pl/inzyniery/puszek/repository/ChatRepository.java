package pl.inzyniery.puszek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import pl.inzyniery.puszek.api.chat.GetChatListRequest;
import pl.inzyniery.puszek.model.Chat;

import java.util.List;

@Component
public interface ChatRepository extends JpaRepository<Chat, Integer> {


    @Query("SELECT chat FROM Chat chat")
    List<Chat> getChatList(@Param("params") GetChatListRequest params);

    Chat findById(@Param("name") int id);
}
