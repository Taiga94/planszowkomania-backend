package pl.inzyniery.puszek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.inzyniery.puszek.model.JoinUserChat;

public interface JoinUserChatRepository extends JpaRepository<JoinUserChat, Integer> {

}
