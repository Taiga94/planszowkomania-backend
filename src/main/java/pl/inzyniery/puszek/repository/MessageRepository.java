package pl.inzyniery.puszek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.inzyniery.puszek.model.MessageModel;

public interface MessageRepository extends JpaRepository<MessageModel, Integer> {
}
