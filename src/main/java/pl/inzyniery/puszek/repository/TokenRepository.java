package pl.inzyniery.puszek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import pl.inzyniery.puszek.model.Token;

@Component
public interface TokenRepository extends JpaRepository<Token, Integer> {
    Token findByContent(@Param("content") String content);
}
