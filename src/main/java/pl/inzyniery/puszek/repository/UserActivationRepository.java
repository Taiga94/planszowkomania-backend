package pl.inzyniery.puszek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import pl.inzyniery.puszek.model.UserActivation;


@Component
public interface UserActivationRepository extends JpaRepository<UserActivation, Integer> {
    public UserActivation findByActivationCode(@Param("activationCode") String code);
}
