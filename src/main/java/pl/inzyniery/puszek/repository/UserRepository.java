package pl.inzyniery.puszek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import pl.inzyniery.puszek.model.Token;
import pl.inzyniery.puszek.model.User;

import java.util.List;

@Component
public interface UserRepository extends JpaRepository<User, Integer> {
    List<User> findById(long id);

    @Query("SELECT DISTINCT u FROM UserModel u")
    List<User> getUsers();

    @Query("SELECT DISTINCT u FROM UserModel u WHERE u.name LIKE CONCAT(:name,'%')")
    List<User> getUsersLookup(@Param("name") String name);

    @Query(value = "SELECT u FROM UserModel u WHERE (SQRT((:latitudeSource - u.latitude) * (:latitudeSource - u.latitude) + (:longitudeSource - u.longitude) * (:longitudeSource - u.longitude)))*111.196672 < :distanceSource")
    List<User> getUsersGPS(@Param("distanceSource") double distanceSource, @Param("latitudeSource") double latitudeSource, @Param("longitudeSource") double longitudeSource);

    User findByLogin(@Param("login") String login);

    User findByEmail(@Param("email") String email);

    User findByUid(@Param("uid") String uid);

    User getByTokens(Token token);
}
