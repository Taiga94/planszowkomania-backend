package pl.inzyniery.puszek.util;

import pl.inzyniery.puszek.model.User;
import pl.inzyniery.puszek.repository.UserRepository;

public class UserUtil {

    public static User getUserByEmailOrLogin(UserRepository repository, String loginOrEmail) {
        User user;
        user = repository.findByEmail(loginOrEmail);
        if (user == null) {
            user = repository.findByLogin(loginOrEmail);
        }

        return user;
    }
}
